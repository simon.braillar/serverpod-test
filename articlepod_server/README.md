# articlepod_server

This is the starting point for your Serverpod server.
    
To generate the files:
```shell 
serverpod generate
```


To run your server, you first need to start Postgres and Redis. It's easiest to do with Docker.
```shell 
docker compose up --build --detach
```

Then you can start the Serverpod server.
    
```shell 
dart bin/main.dart
```

When you are finished, you can shut down Serverpod with `Ctrl-C`, then stop Postgres and Redis.

```shell 
docker compose stop
```
    
