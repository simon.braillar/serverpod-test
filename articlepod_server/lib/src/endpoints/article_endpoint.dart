import 'package:articlepod_server/src/generated/article.dart';
import 'package:serverpod/serverpod.dart';

class ArticleEndpoint extends Endpoint {

  // Fetch articles
  Future<List<Article>> getArticles(Session session, {String? keyword}) async {
    session.log("Fetching articles ${(session as MethodCallSession).httpRequest.remoteIpAddress}", level: LogLevel.info);
    return await Article.find(session,
      where: (t) => keyword != null ? t.title.like('%$keyword%') : Constant(true),
    );
  }

  // Add a new article
  Future<bool> addArticle(Session session, {required Article article}) async {
    Article.insert(session, article);

    return true;
  }

  // Update an article
  Future<bool> updateArticle(Session session, {required Article article}) async {
    var result = await Article.update(session, article);
    return result;
  }

  // Delete an article
  Future<bool> deleteArticle(Session session, {required int id}) async {
    var result = await Article.delete(session, where: (t) => t.id.equals(id));
    return result == 1;
  }

}